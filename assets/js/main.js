$(document).ready(function () {


    $(document).on('click', '.product-small-imgs .more', function () {
        $(".slider-modal,.overflow").addClass('active')
    })
    $(document).on('click', '.slider-modal #close-this', function () {
        $(".slider-modal").removeClass('active')
        $('.page-overflow').removeClass('open')
    })
    $(document).on('focus', '.login-page-content form input', function () {
        $(this).parent().addClass('is-focus')
    })
    $(document).on('blur', '.login-page-content form input', function () {
        if($(this).val().length < 1) {
            $(this).parent().removeClass('is-focus')
        }  
    })
    $(document).on('focus', '.register-page-content form input', function () {
        $(this).parent().addClass('is-focus')
    })
    $(document).on('blur', '.register-page-content form input', function () {
        if ($(this).val().length < 1) {
            $(this).parent().removeClass('is-focus')
        }
    })


    $(document).on('click', '.product-tc-item-res', function () {
        var nav = $(this).parent(),
            animateTime = 800;
        console.log(nav.height() )
        if (nav.height() === 80) {
            nav.addClass('open')
            autoHeightAnimate(nav, animateTime);
        } else {
            nav.removeClass('open');
            nav.stop().animate({ height: '80' }, animateTime);
        }
    })
         

    // 
    // ****** Change Tabs in product page
    //
    $(document).on('click','.profuct-tl-item',function(){
        if($(this).hasClass('active'))
            return true
        else{
            $('.profuct-tl-item').each(function(){
                $(this).removeClass('active') ;
            })
            $('.product-tc-item').each(function () {
                $(this).removeClass('active');
            })   
            $(this).addClass('active')
            var selectedTabId = $(this).attr('data-id')
            $('.product-tc-item').each(function () {
                if($(this).attr('data-id') === selectedTabId ){
                    $(this).addClass('active')
                }
            })         
        }
    })

    // 
    // ****** go to top of the page when we click on back to top btn
    //
    $(document).on('click', '.back-to-top', function () {  
        $('body,html').animate({
            scrollTop: 0
        }, 1200);
    })


    // 
    // ****** Change Selected Color for Product in product page
    // 
    $(document).on('click', '.product-color-item', function (event) {
        // if the color already selected return true
        if($(this).hasClass('active'))
            return true ;
        // if color is not selected , remove seleced ( active class ) from all colors and add active class to this color
        else{
            $('.product-color-item').each(function () {
                $(this).removeClass('active') ;
            })
            $(this).addClass('active')
        }
    })
 

    $(document).on('click','.product-archive-sidebar-holder .product-as-title',function(){
         
        console.log(4)
        var nav = $(this).parent() ,
            animateTime = 800;
        console.log(4, nav.height() )
        if (nav.height() === 50) {
            nav.addClass('open')
            autoHeightAnimate(nav, animateTime);
        } else {
            nav.removeClass('open');
            nav.stop().animate({ height: '50' }, animateTime);
        }
    })
    $(document).on('click', '.res-filter-items .product-as-title', function () {
         
        var nav = $(this).parent(),
            animateTime = 800;
         if (nav.height() === 48) {
            console.log(2)
            nav.addClass('open')
            autoHeightAnimate(nav, animateTime);
        } else {
            console.log(3)
            nav.removeClass('open');
            nav.stop().animate({ height: '50' }, animateTime);
        }
    })
    $(document).on('click', '.product-archive-items-filter span', function () {
        if($(this).hasClass('active'))
            return false
        else{
            $('.product-archive-items-filter span').each(function(){
                $(this).removeClass('active')
            })
            $(this).addClass('active')
        }
    })

    $(document).on('click', '#open-filter-sidebar', function () {
        $('.res-filter-sidebar-holder').addClass('open')
    })
    $(document).on('click', '#close-filter-sidebar', function () {
        $('.res-filter-sidebar-holder').removeClass('open')
    })


    // 
    // ****** Toggle Brand in product archive page
    // 
    $(document).on('click', '.navigation-btn', function (event) {
        $('.responsive-menu').addClass('open')
        $('.page-overflow').addClass('open')
    })
    $(document).on('click', '.page-overflow', function (event) {
        $('.responsive-menu').removeClass('open')
        $(".slider-modal").removeClass('active')
        $('.page-overflow').removeClass('open')
    })



    // 
    // ****** Toggle Brand in product archive page
    // 
    $(document).on('click', '.responsive-menu .has-submenu>a', function (event) {
        event.preventDefault();
        console.log(1)
        if ($(this).parent().find('ul')) {
            $('.responsive-menu .has-submenu ul').each(function () {
                
                  $(this).stop().animate({ height: '0' }, animateTime);  
                
            })
            var nav = $(this).parent().find('>ul'),
                animateTime = 800;
            if (nav.height() === 0) {
                $(this).addClass('active')
                autoHeightAnimate(nav, animateTime);
            } else {
                $(this).removeClass('active')
                nav.stop().animate({ height: '0' }, animateTime);
            }
        }
        else {
            return true
        }

    })
    $(document).on('click', '.responsive-menu .leveltwo>a', function (event) {
        event.preventDefault();
        $('.responsive-menu .has-submenu ul').each(function () {
           
                $(this).stop().animate({ height: '0' }, 0);
            
        })
        $(this).parent().parent().stop().animate({ height: parentHeight }, 0);
        if ($(this).parent().find('>ul')) {
            var parentHeight = $(this).parent().parent()
            var nav = $(this).parent().find('>ul'),
                animateTime = 800;
            if (nav.height() === 0) {

                autoHeightAnimate(nav, animateTime);
                $(this).parent().parent().css({ height: 'auto' })
            } else {

                nav.stop().animate({ height: '0' }, animateTime);
                $(this).parent().parent().stop().animate({ height: parentHeight }, animateTime);

            }
        }
        else {
            return true
        }

    })


    // 
    // ****** Toggle Brand in product archive page
    // 
    $(document).on('click', '.product-as-brand-item', function () {
        if ($(this).hasClass('active'))
            $(this).removeClass('active')
        else
            $(this).addClass('active')
    })



    // 
    // ****** Toggle Brand in product archive page
    // 
    $(document).on('click', '.product-as-color-item', function () {
        if ($(this).hasClass('active'))
            $(this).removeClass('active')
        else
            $(this).addClass('active')
    })



    // 
    // ****** Toggle Brand in product archive page
    // 
    $(document).on('click', '#statusswitcher', function () {
        $(this).toggleClass('active')
    })


    // set height equal to width
    $('.qualification-item').each(function () {
        var width = $(this).width()
        $(this).height(width)
    })


    $(document).on('mouseover', '.submenu-item', function () {
        $('.submenu-item').each(function () {
            $(this).removeClass('is-active');
            $(this).find('.submenuitem-childs').removeClass('is-active')
        })
        $(this).addClass('is-active');
        $(this).find('.submenuitem-childs').addClass('is-active')
    })

    $(document).on('mouseover', '.main-nav ul li', function () {
        if ($(this).hasClass('has-submenu')) {
            $('.page-overflow').addClass('is-active').delay(500).queue(function () {
                $(this).find('.submenu').addClass('is-active');
                $(this).dequeue()
            })
            $(this).find('.submenu').addClass('is-active');
        }

    })

    $(document).on('mouseleave', '.main-nav ul li', function () {
        $('.page-overflow').removeClass('is-active')
        $(this).find('.submenu').removeClass('is-active');
    })



    // 
    // ****** do this actions on window resize
    // 

    $(window).resize(function () {
        // set height equal to width for qualification items in home page
        $('.qualification-item').each(function () {
            var width = $(this).width()
            $(this).height(width)
        })
    })


    // 
    // ****** do this actions on window scroll
    // 

    $(window).scroll(function () {
        let windowScroll = $(this).scrollTop()

        if(windowScroll > 800 ) 
            $('.back-to-top').addClass('active')
        else 
            $('.back-to-top').removeClass('active')
    })


    var swiperSpecial = new Swiper('.discount-slider.special .swiper-container', {
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        loop: true,
        navigation: {
            nextEl: '.discount-slider.special .swiper-button-next',
            prevEl: '.discount-slider.special .swiper-button-prev',
        },
    });
    var swiperMain = new Swiper('.big-slider .swiper-container', {
        
        navigation: {
            nextEl: '.big-slider .swiper-button-next',
            prevEl: '.big-slider .swiper-button-prev',
        },
    });
    var swiperpTopDiscounts = new Swiper('.discount-slider.top-discounts .swiper-container', {
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        loop:true,
        navigation: {
            nextEl: '.discount-slider.top-discounts .swiper-button-next',
            prevEl: '.discount-slider.top-discounts .swiper-button-prev',
        },
    });

    var swiperSoon = new Swiper('.main-slider.soon .swiper-container', {
        slidesPerView: 6,
        spaceBetween: 20,
        navigation: {
            nextEl: '.main-slider.soon .swiper-button-next',
            prevEl: '.main-slider.soon .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            478: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });
    var swiperNewest = new Swiper('.main-slider.newest .swiper-container', {
        slidesPerView: 6,
        spaceBetween: 20,
        navigation: {
            nextEl: '.main-slider.newest .swiper-button-next',
            prevEl: '.main-slider.newest .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            478: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });
    var swiperRelated = new Swiper('.related-product-slider .swiper-container', {
        slidesPerView: 6,
        spaceBetween: 20,
        navigation: {
            nextEl: '.related-product-slider .swiper-button-next',
            prevEl: '.related-product-slider .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            478: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });
    var swiperMostSell = new Swiper('.main-slider.most-sell .swiper-container', {
        slidesPerView: 6,
        spaceBetween: 20,
        navigation: {
            nextEl: '.main-slider.most-sell .swiper-button-next',
            prevEl: '.main-slider.most-sell .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            478: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });
    var swiperBrands = new Swiper('.main-slider.brands .swiper-container', {
        slidesPerView: 6,
        spaceBetween: 20,
        navigation: {
            nextEl: '.main-slider.brands .swiper-button-next',
            prevEl: '.main-slider.brands .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            478: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });

})



/* Function to animate height: auto */
function autoHeightAnimate(element, time) {
    var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
    element.height(curHeight); // Reset to Default Height
    element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height
}


//************  
//************ map functions
//************ 
function myMap() {
    // This example displays a marker at the center of Australia.
    // When the user clicks the marker, an info window opens.
    var uluru = { lat: 35.6891980, lng: 51.3889740 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: uluru,
        mapTypeControl: false,
        disableDefaultUI: true
    });

 

 
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        title: 'Uluru (Ayers Rock)'
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    
    google.maps.event.addListener(map, 'click', function (e) {
        var lat = e.latLng.lat(); var lng = e.latLng.lng();
        $('#lat').val(lat);
        $('#lng').val(lng);
    });
}